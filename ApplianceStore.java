import java.util.Scanner;

public class ApplianceStore
{
	public static void main (String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		DishWasher[] Appliances = new DishWasher[4];
		
		for (int i = 0; i<Appliances.length; i++)
		{
			Appliances[i] = new DishWasher();
			
			System.out.print("Please enter the Size of the Dish Washer in cm^3: ");
			Appliances[i].size = sc.nextInt();
			System.out.print("Please enter the Brand of the Dish Washer: ");
			Appliances[i].brand = sc.next();
			System.out.print("Please enter the Price of the Dish Washer: ");
			Appliances[i].price = sc.nextDouble();
		}
		System.out.println(Appliances[3]);
		
		System.out.println(Appliances[0].printBrand());
		System.out.println(Appliances[0].fastClean());
	}
}